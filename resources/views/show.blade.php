@extends ('layouts.app')

@section ('title', 'Проект -' . $project->title)

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="projects">
        <div class="projects-content">
            <div class="projects-content_header">
                <h1>Проект - {{ $project->title }}</h1>
            </div>

            <div class="projects-content-block">

                <div class="projects-content__prop">
                    Статус:
                    @if ($project->isActive())
                        <span class="badge badge-success">
                            Активный
                        </span>
                    @elseif ($project->isCompleted())
                        <span class="badge badge-primary">
                            Завершен
                        </span>
                    @elseif ($project->isCancelled())
                        <span class="badge badge-danger">
                            Отменен
                        </span>
                    @endif
                </div>

                <div class="projects-content__prop">
                    Бюджет: <span class="__price">{{ $project->getBudgetFormat() }}</span>
                </div>

                <div class="projects-content__prop">
                    Клиент:  {{ $project->getClientNoFormat() }}
                </div>

                <div class="projects-content__prop">
                    Описание:
                    <div class="projects-content__description">
                        {{ $project->description }}
                    </div>
                </div>

                <div class="projects-content__prop">
                    <a class="btn btn-alt-primary" href="{{ route('project.edit', $project) }}">
                        Редактировать
                    </a>

                    <a class="btn btn-alt-danger" href="{{ route('project.index') }}">
                        Назад
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection
