@extends ('layouts.app')

@section ('title', 'Проекты')

@section ('content')
    <div class="projects">
        <div class="projects-control d_flex">
            <div class="col col_6 col_lg-3">
                <a class="projects-control_block __green" href="{{ route('project.create') }}">
                    <div class="projects-control_block-content_full">
                        <i class="fa fa-plus"></i>
                    </div>

                    <div class="projects-control_block-content_short">
                        Добавить проект
                    </div>
                </a>
            </div>
        </div>

        <div class="projects-content">
            <div class="projects-content_header">
                <h1>Проекты</h1>
            </div>

            <div class="projects-content-block">
                <form class="projects-content-block-search" action="?" method="GET">
                    <div class="form-group">
                        <div class="input-group">
                            <label for="products-search" class="form-label"><span>Поиск:</span>
                                <input type="search" class="form-control projects-content-block-search__input" id="projects-search" name="projects_search" placeholder="Поиск проекта" autocomplete="off" value="{{ $filter }}">
                            </label>

                            <button class="projects-content-block-search__btn" type="submit" title="Поиск">
                                <i class="fa fa-search"></i>
                            </button>
                        </div>
                    </div>
                </form>

                <div class="table-responsive">
                    <table class="table table-borderless table-striped table-vcenter">
                        <thead>
                            <tr>
                                <th class="text-center">
                                    ID
                                </th>

                                <th>
                                    Проект
                                </th>

                                <th class="text-right">
                                    Бюджет
                                </th>

                                <th class="text-right">
                                    Клиент
                                </th>

                                <th class="text-center">
                                    Дата добавления
                                </th>

                                <th>
                                    Статус
                                </th>

                                <th class="text-center" style="font-size: 18px;">
                                    <i class="fas fa-cogs"></i>
                                </th>
                            </tr>
                        </thead>

                        <tbody>
                        @foreach($projects as $item)
                            <tr>
                                <td class="text-center">
                                    <a href="{{ route('project.edit', $item) }}">
                                        <strong>
                                            {{ $item->id }}
                                        </strong>
                                    </a>
                                </td>

                                <td>
                                    <a href="{{ route('project.edit', $item) }}">
                                        {{ $item->title }}
                                    </a>
                                </td>

                                <td class="text-right __price">
                                    {{ $item->getBudgetFormat() }}
                                </td>

                                <td class="text-right">
                                    {{ $item->getClientNoFormat() }}
                                </td>

                                <td class="text-center">
                                    {{ $item->created_at->format('d.m.Y') }}
                                </td>

                                <td>
                                    @if ($item->isActive())
                                        <span class="badge badge-success">
                                            Активный
                                        </span>
                                    @elseif ($item->isCompleted())
                                        <span class="badge badge-primary">
                                            Завершен
                                        </span>
                                    @elseif ($item->isCancelled())
                                        <span class="badge badge-danger">
                                            Отменен
                                        </span>
                                    @endif
                                </td>

                                <td class="__settings text-center d_flex grid_center">
                                    <a class="btn btn-alt-primary" href="{{ route('project.show', $item) }}">
                                        <i class="fa fa-fw fa-eye"></i>
                                    </a>

                                    <a class="btn btn-alt-primary" href="{{ route('project.edit', $item) }}">
                                        <i class="fas fa-pencil-alt"></i>
                                    </a>

                                    <form method="post" action="{{ route('project.destroy', $item) }}">
                                        @csrf
                                        @method('DELETE')

                                        <button class="btn btn-alt-primary">
                                            <i class="fa fa-fw fa-times"></i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

                <nav aria-label="Navigation">
                    {{ $projects->withQueryString()->links() }}
                </nav>
            </div>
        </div>
    </div>
@endsection
