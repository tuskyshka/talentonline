<!doctype html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>
        {{ config('app.name') }} - @yield('title')
    </title>

    <link rel="shortcut icon" href="/build/images/favicon.png">

    <link rel="stylesheet" href="/build/css/screen.css">
</head>
<body class="home">

    <main class="main">
        @include('layouts.partials.flash')

        @yield('content')
    </main>


    <script src="/build/js/main.js"></script>
</body>
</html>
