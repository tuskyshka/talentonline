@extends ('layouts.app')

@section ('title', 'Создать проект')

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="projects">
        <div class="projects-content">
            <div class="projects-content_header">
                <h1>Создание проекта</h1>
            </div>

            <div class="projects-content-block">
                <form method="post" action="{{ route('project.store') }}" class="pt-3">
                    @csrf

                    <div class="form-group row">
                        <label for="status" class="col_form-label">
                            Статус
                        </label>

                        <select class="form-control" id="status" name="status">
                            @foreach ($status as $key => $value)
                                <option value="{{$key}}">
                                    {{ $value }}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group row">
                        <label for="title" class="col_form-label">
                            Название проекта*
                        </label>

                        <input type="text" name="title" class="form-control" id="title" placeholder="example.ru" required>
                    </div>

                    <div class="form-group row">
                        <label for="offer" class="col_form-label">
                            Бюджет
                        </label>

                        <input type="text" name="offer" class="form-control" id="offer" placeholder="100 000">
                    </div>

                    <div class="form-group row">
                        <label for="client" class="col_form-label">
                            Клиент
                        </label>

                        <input type="text" name="client" class="form-control" id="client" placeholder="ООО 'Рога и Копыта'" value="" autocomplete="off">

                        <div id="client-list" class="client__list"></div>
                    </div>

                    <div class="form-group row">
                        <label for="description" class="col_form-label">
                            Описание
                        </label>

                        <textarea name="description" class="form-control __textarea" id="description" rows="4"></textarea>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-10">
                            <button type="submit" name="save" class="btn btn-alt-primary">
                                Добавить
                            </button>

                            <button type="submit" name="apply" class="btn btn-alt-success">
                                Применить
                            </button>

                            <a class="btn btn-alt-danger" href="{{ route('project.index') }}">
                                Отменить
                            </a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
