// webpack v.4 config
const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const FixStyleOnlyEntriesPlugin = require("webpack-fix-style-only-entries");
const MinifyPlugin = require('babel-minify-webpack-plugin');
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const CopyPlugin = require("copy-webpack-plugin");
const CleanWebpackPlugin = require('clean-webpack-plugin');
const webpack = require("webpack");

let config = (env) => {
    let is_development = (env === 'development');
    let is_production = !is_development;
    let dist_path = path.join(__dirname, '/build');

    return {
        entry: {
            main: './src/assets/js/index.js',
            screen: './src/assets/styles/style.scss'
        },
        output: {
            path: dist_path,
            filename: 'js/[name].js',
            chunkFilename: 'js/[name].js',
            publicPath: '/build/'
        },
        module: {
            rules: [
                {
                    test: /\.js$/,
                    exclude: ['/node_modules/'],
                    use: [{
                        loader: 'babel-loader'
                    }]
                },
                {
                    test: /\.scss$/,
                    exclude: /node_modules/,
                    use: [
                       MiniCssExtractPlugin.loader,
                        'css-loader',
                        {
                            loader: 'postcss-loader',
                            options: {
                                plugins: [
                                    is_production ? require('cssnano') : () => {},
                                ]
                            }
                        },
                        'sass-loader',
                    ],
                },
                {
                    test: /\.(gif|png|jpe?g|ico)$/i,
                    use: [{
                        loader: 'file-loader',
                        options: {
                            name: 'images/[name].[ext]'
                        }
                    }, {
                        loader: 'image-webpack-loader',
                        options: {
                            mozjpeg: {
                                progressive: true,
                                quality: 70
                            }
                        }
                    }],
                },
                {
                    test: /\.svg$/i,
                    use: [{
                        loader: 'file-loader',
                        options: {
                            name: 'svg/[name].[ext]'
                        }
                    }],
                },
                {
                    test: /\.(eot|ttf|woff|woff2)$/,
                    use: {
                        loader: 'file-loader',
                        options: {
                            name: 'fonts/[name].[ext]'
                        }
                    },
                }
            ],
        },
        plugins: [
            new MinifyPlugin(),
            new FixStyleOnlyEntriesPlugin(),
            new MiniCssExtractPlugin({
                filename: 'css/[name].css',
                chunkFilename: 'css/[name].css'
            }),
            new CopyPlugin([
                {
                    from: './src/assets/svg/',
                    to: './svg/',
                },
                {
                    from: './src/assets/images/',
                    to: './images/',
                },
                {
                    from: './src/assets/fonts/',
                    to: './fonts/',
                },
            ]),
            new CleanWebpackPlugin(['build']),
            new webpack.ProvidePlugin({}),
        ],
        optimization: is_production ? {
            minimizer: [
                new OptimizeCSSAssetsPlugin({})
            ],
        } : {},
       devtool: is_production ? false : 'eval-source-map',
        watch: is_development,
        watchOptions: {
            aggregateTimeout: 500,
            ignored: '/node_modules/',
        },
    }
};

module.exports = config;
