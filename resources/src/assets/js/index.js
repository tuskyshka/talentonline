import Client from './modules/Client';
import Ckeditor from './modules/Ckeditor';
import Mask from './modules/Mask';

new Client();
new Ckeditor();
new Mask();

