import ClassicEditor from '@ckeditor/ckeditor5-build-classic';

function Ckeditor() {
    ClassicEditor
        .create( document.querySelector( '#description' ) )
        .then( editor => {
            console.log( editor );
        })
        .catch( error => {
            console.error( error );
        });
}

export default Ckeditor();
