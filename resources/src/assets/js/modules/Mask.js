import IMask from 'imask';

function Mask() {

    let $input = document.getElementById('offer');

    let $mask = {
        mask: 'num',
        blocks: {
            num: {
                mask: Number,
                thousandsSeparator: ' '
            }
        }
    };

    IMask($input, $mask);
}

export default Mask();
