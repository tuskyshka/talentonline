
function Client() {
    this.init();
}

Client.prototype = {
    url: '/api/client',
    input: document.getElementById('client'),
    client_list: document.getElementById('client-list'),
    response: null,

    init: function() {
        this.input.oninput = (e) => {
            if (this.input.value.length > 3) {
                this.post(this.input.value);
                this.html();
                this.clientSelect()
            } else {
                this.client_list.classList.remove('__active');
            }
        };

        // this.input.onblur = (e) => {
        //     this.client_list.classList.remove('__active');
        // };
    },

    post: function (params) {
        fetch(this.url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify({client: params})
        }).then((response) => {
            response.json().then((data) => {
                this.response = data;
            });
        });
    },

    html: function () {
        let html = '';

        if (this.response !== null) {
            if (this.response.status === 200) {
                html += '<ul>';
                this.response.data.forEach(function (item, i, arr) {
                    html += '<li class="client__list_item">' + item.name + ' [' + item.id + ']' + '</li>';
                });
                html += '</ul>';

                this.client_list.classList.add('__active');
                this.client_list.innerHTML = html;
            }
        }
    },

    clientSelect: function () {
        if (this.response.status === 200) {
            this.client_list.firstChild.onclick = (e) => {
                this.input.value = e.target.innerHTML;
                this.client_list.classList.remove('__active');
            }
        }
    }
}

export default Client;
