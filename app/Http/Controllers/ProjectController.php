<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Http\Requests\Project\ProjectRequest;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $filter = $request->input('projects_search');

        $projects = Project::orderBy('id')
            ->when(!empty($value = $filter), function($query) use ($value) {
                $query->where('title', 'like', '%' . $value . '%');
            })
            ->paginate(20);

        return view('index', compact('projects', 'filter'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $status = Project::getStatusList();

        return view('create', compact('status'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProjectRequest $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(ProjectRequest $request)
    {
        $project = Project::create($request->validated());

        if (isset($request['save'])) {
            return  redirect(route('project.index'));
        }

        return  redirect(route('project.edit', $project));
    }

    /**
     * Display the specified resource.
     *
     * @param Project $project
     * @return string
     */
    public function show(Project $project)
    {
        $status = Project::getStatusList();

        return view('show', compact('project', 'status'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Project $project
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Project $project)
    {
        $status = Project::getStatusList();

        return view('edit', compact('project', 'status'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ProjectRequest $request
     * @param Project $project
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(ProjectRequest $request, Project $project)
    {
        $project->update($request->validated());

        if (isset($request['save'])) {
            return  redirect(route('project.index'));
        }

        return  redirect(route('project.edit', $project));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Project $project
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function destroy(Project $project)
    {
        $project->delete();

        return redirect(route('project.index'));
    }
}
