<?php

namespace App\Http\Controllers\Api;

use App\Models\Client;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getClientsByName(Request $request)
    {
        $filter = $request->input('client');

        $clients = Client::orderBy('id')
            ->when(!empty($value = $filter), function($query) use ($value) {
                $query->where('name', 'like', '%' . $value . '%');
            })
            ->paginate(5);

        if ($clients->count()) {
            return response()->json([
                'status' => Response::HTTP_OK,
                'data' => $clients->all(),
            ], Response::HTTP_OK);
        }

        return response()->json([
            'status' => Response::HTTP_NOT_FOUND,
            'message' => 'Clients not found',
            'errors' => [],
        ], Response::HTTP_NOT_FOUND);
    }
}
