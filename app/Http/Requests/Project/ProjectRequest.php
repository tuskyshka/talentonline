<?php

namespace App\Http\Requests\Project;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Models\Project;

class ProjectRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    protected function prepareForValidation()
    {
        $this->request->set('offer', str_replace(' ', '', $this->request->get('offer')));

        preg_match('/([0-9]+)/', $this->request->get('client'), $matches);
        $this->request->set('client', $matches[0]);
    }

    public function rules(): array
    {
        return [
            'title' => 'required|string|max:255',
            'status' => [
                'required',
                Rule::in(array_flip(Project::getStatusList())),
            ],
            'offer' => 'nullable',
            'client' => 'nullable',
            'description' => 'nullable|string',
        ];
    }
}
