<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Project
 *
 * @property int $id
 * @property string $title
 * @property int $offer
 * @property string $company
 * @property string $client
 * @property string $status
 * @property string $description
 * @property string $created_at
 * @property string $updated_at
 *
 * @package App\Models
 */
class Project extends Model
{
    public const STATUS_CANCELLED = 'cancelled';
    public const STATUS_COMPLETED = 'completed';
    public const STATUS_ACTIVE = 'active';

    protected $guarded = [];

    /**
     * Получаем массив статусов
     *
     * @return string[]
     */
    public static function getStatusList(): array
    {
        return [
            self::STATUS_ACTIVE => 'Активен',
            self::STATUS_COMPLETED => 'Завершен',
            self::STATUS_CANCELLED => 'Отменен',
        ];
    }

    /**
     * Получаем статус по его коду
     *
     * @param $status
     * @return string
     */
    public static function getStatus($status): string
    {
        $data = [
            self::STATUS_ACTIVE => 'Активен',
            self::STATUS_COMPLETED => 'Завершен',
            self::STATUS_CANCELLED => 'Отменен',
        ];

        return $data[$status];
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->status === self::STATUS_ACTIVE;
    }

    /**
     * @return bool
     */
    public function isCompleted(): bool
    {
        return $this->status === self::STATUS_COMPLETED;
    }

    /**
     * @return bool
     */
    public function isCancelled(): bool
    {
        return $this->status === self::STATUS_CANCELLED;
    }

    /**
     * @return string
     */
    public function getBudgetFormat(): string
    {
        return number_format($this->offer, '0', '.', ' ') . ' р.';
    }

    public function getClient()
    {
        return $this->hasOne(Client::class, 'id', 'client');
    }

    public function getClientFormat()
    {
        $client = $this->getClient()->first()->toArray();

        return $client['name'] . ' [' . $client['id'] . ']';
    }

    public function getClientNoFormat()
    {
        $client = $this->getClient()->first()->toArray();

        return $client['name'];
    }
}
