<?php

use Illuminate\Support\Facades\Route;

Route::redirect('/', '/project');

Route::resources([
    'project' => 'App\Http\Controllers\ProjectController',
]);
